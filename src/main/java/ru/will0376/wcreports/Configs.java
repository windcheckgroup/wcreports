package ru.will0376.wcreports;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import scala.actors.threadpool.Arrays;

import java.util.List;

@Config(modid = WCReports.MOD_ID)
@Mod.EventBusSubscriber(modid = WCReports.MOD_ID)
public class Configs {
	@Config.Comment("Black list of players")
	public static String[] blacklist = new String[]{};
	public static String whitelistPermission = "wind.reports.whitelist";
	public static String whitelistBypassPermission = "wind.reports.whitelistbypass";
	public static String cooldownBypassPermission = "wind.reports.cooldownbypass";
	public static String playerCanReadReports = "wind.reports.canreadreports";

	@Config.Comment("Cooldown in min")
	public static int cooldown = 10;

	@SubscribeEvent
	public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
		if (event.getModID().equals(WCReports.MOD_ID)) {
			ConfigManager.sync(WCReports.MOD_ID, Config.Type.INSTANCE);
		}
	}

	public static void addToBlackList(String in) {
		List<String> list = Arrays.asList(blacklist);
		list.add(in);
		blacklist = list.toArray(new String[0]);
		ConfigManager.sync(WCReports.MOD_ID, Config.Type.INSTANCE);
	}

	public static void removeFromBlackList(String in) {
		List<String> list = Arrays.asList(blacklist);
		list.remove(in);
		blacklist = list.toArray(new String[0]);
		ConfigManager.sync(WCReports.MOD_ID, Config.Type.INSTANCE);
	}
}
