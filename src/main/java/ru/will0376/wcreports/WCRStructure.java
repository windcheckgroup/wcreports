package ru.will0376.wcreports;

import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.events.req.RequestScreenEvent;

public class WCRStructure {
	public String playerNick;
	public String adminNick;
	public long time;

	public WCRStructure(String player, String customer) throws Exception {
		if (player == null)
			throw new NullPointerException("Player not found");
		if (checkWhiteList(customer, playerNick))
			throw new Exception("You have no rights to this action!");
		this.time = System.currentTimeMillis();
		this.playerNick = player;
		this.adminNick = customer;
	}

	/**
	 * return true if cooldown enable.
	 */
	public boolean cooldown() {
		if (adminNick.equals("Server"))
			return false;
		if (playerHasPerm(adminNick, Configs.cooldownBypassPermission))
			return false;
		return time + (Configs.cooldown * 60000L) > System.currentTimeMillis();
	}

	public void makeScreen() {
		WindCheckBridge.aVersion.createNewToken(playerNick, adminNick, WCReports.requester);
		WindCheckBridge.aVersion.sendEvent(new RequestScreenEvent(adminNick, WCReports.requester, playerNick));
	}

	/**
	 * @return true, if player in WL
	 */
	private boolean checkWhiteList(String customer, String player) {
		if (customer.equals("Server"))
			return false;
		if (playerHasPerm(customer, Configs.whitelistBypassPermission))
			return false;
		return playerHasPerm(player, Configs.whitelistPermission);
	}

	private boolean playerHasPerm(String player, String perm) {
		return WindCheckBridge.aVersion.senderCanUseCommand(WindCheckBridge.aVersion.findPlayer(player), perm);
	}
}
