package ru.will0376.wcreports;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.utils.Module;
import ru.will0376.windcheckbridge.utils.SubCommands;
import ru.will0376.windcheckbridge.utils.Token;

@Mod(modid = WCReports.MOD_ID,
		name = WCReports.MOD_NAME,
		version = WCReports.VERSION,
		acceptedMinecraftVersions = "[1.12.2]",
		dependencies = "after:windcheckbridge@[3.0,)",
		acceptableRemoteVersions = "*")
public class WCReports {

	public static final String MOD_ID = "wcreports";
	public static final String MOD_NAME = "WCReports";
	public static final String VERSION = "@version@";

	@Mod.Instance(MOD_ID)
	public static WCReports INSTANCE;

	public static Module module = Module.builder().modid(MOD_ID).setDefaultNames(MOD_NAME).version(VERSION).build();
	public static SubCommands command;
	public static Token.Requester requester;

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		requester = WindCheckBridge.aVersion.registerModule(module);
		command = WindCheckBridge.aVersion.registerNewCommand(new ReportCommand());
	}

	@Mod.EventHandler
	public void serverStartedEvent(FMLServerStartingEvent event) {
		event.registerServerCommand(new PlayerCommand());
	}

}
