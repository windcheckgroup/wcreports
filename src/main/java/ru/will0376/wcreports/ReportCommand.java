package ru.will0376.wcreports;

import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import org.apache.commons.cli.CommandLine;
import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.utils.AbstractCommand;

import java.util.Arrays;
import java.util.List;

public class ReportCommand extends AbstractCommand {
	public ReportCommand() {
		super("report");
	}

	@Override
	public List<Argument> getArgList() {
		return Arrays.asList(getArgBuilder("blacklistAdd").setHasArg().build(), getArgBuilder("blacklistRemove").setHasArg()
				.build());
	}

	@Override
	public ActionStructure execute(MinecraftServer server, ICommandSender sender, String[] args) throws Exception {
		ActionStructure build = getStartedAction().args(args).adminNick(sender.getName()).build();

		if (!WindCheckBridge.aVersion.senderCanUseCommand(sender, getPermission())) {
			return build.setCanceled("no rights");
		}

		CommandLine line = getLine(args);
		if (line.hasOption("blacklistAdd")) {
			Configs.addToBlackList(line.getOptionValue("blacklistAdd"));
		} else if (line.hasOption("blacklistRemove")) {
			Configs.removeFromBlackList(line.getOptionValue("blacklistRemove"));
		}
		return build;
	}
}
