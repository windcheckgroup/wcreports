package ru.will0376.wcreports;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import ru.will0376.windcheckbridge.WindCheckBridge;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class PlayerCommand extends CommandBase {
	public static HashMap<String, WCRStructure> cooldownMap = new HashMap<>();

	@Override
	public String getName() {
		return "report";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/report <player>";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length == 0) {
			WindCheckBridge.aVersion.printToSender(sender, getUsage(sender));
		} else {
			try {
				if (Arrays.stream(Configs.blacklist).anyMatch(e -> e.equals(sender.getName()))) {
					WindCheckBridge.aVersion.printErrorToSender(sender, "You're on the blacklist!");
					return;
				}

				if (cooldownMap.containsKey(sender.getName()) && cooldownMap.get(sender.getName()).cooldown()) {
					WindCheckBridge.aVersion.printErrorToSender(sender, String.format("Cooldown: %s sec",
							(cooldownMap.get(sender.getName()).time + (Configs.cooldown * 60000L) - System.currentTimeMillis()) / 1000));
					return;
				}

				WCRStructure wcrStructure = new WCRStructure(args[0], sender.getName());
				cooldownMap.put(server.getName(), wcrStructure);
				wcrStructure.makeScreen();
				WindCheckBridge.aVersion.printToSender(sender, "Thanks!");
			} catch (Exception ex) {
				WindCheckBridge.aVersion.printErrorToSender(sender, ex.getMessage());
				ex.printStackTrace();
			}
		}
	}

	@Override
	public int getRequiredPermissionLevel() {
		return 0;
	}

	@Override
	public List<String> getAliases() {
		return Arrays.asList("rp", "rep");
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args,
			@Nullable BlockPos targetPos) {
		return args.length == 1 ? getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames()) :
				Collections.emptyList();
	}
}
