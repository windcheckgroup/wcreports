package ru.will0376.wcreports;

import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.events.MessageModuleToModuleEvent;
import ru.will0376.windcheckbridge.events.ans.PrintInfoForAnotherRequester;

@Mod.EventBusSubscriber(modid = WCReports.MOD_ID)
public class Events {
	@SubscribeEvent
	public static void anotherRequesterEvent(PrintInfoForAnotherRequester event) {
		if (event.getToken().getRequester().equals(WCReports.requester)) {
			if (WindCheckBridge.aVersion.isModuleLoaded("wcdiscord")) {
				WindCheckBridge.aVersion.sendEvent(new MessageModuleToModuleEvent(WCReports.module,
						WindCheckBridge.aVersion.getLoadedModule("wcdiscord"), "printToReportChannel", String.format("[Report] " +
						"%s -> %s", event.getToken()
						.getPlayerNick(), event.getText())));
			}

			FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers().forEach(playerMP -> {
				if (WindCheckBridge.aVersion.senderCanUseCommand(playerMP, Configs.playerCanReadReports)) {
					WindCheckBridge.aVersion.printToSenderWithClick(playerMP, String.format("{Report} %s -> %s",
							playerMP.getName(), event.getText()), event.getText());
				}
			});
		}
	}
}
